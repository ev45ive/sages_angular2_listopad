import { Angular2listopadPage } from './app.po';

describe('angular2listopad App', function() {
  let page: Angular2listopadPage;

  beforeEach(() => {
    page = new Angular2listopadPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Angular 2');
  });
});
