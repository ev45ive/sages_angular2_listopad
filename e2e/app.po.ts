import { browser, element, by } from 'protractor';

export class Angular2listopadPage {
  navigateTo(path = '/') {
    return browser.get(path);
  }

  getParagraphText() {
    return element(by.css('.placki h3')).getText();
  }
}
