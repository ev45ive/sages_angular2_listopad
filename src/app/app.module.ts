import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MusicAppModule } from './music-app/music-app.module'

import { AppComponent } from './app.component';
import { TodoAppComponent } from './todo-app/todo-app.component';
import { TodoItemComponent } from './todo-app/todo-item.component';
import { TodoListComponent } from './todo-app/todo-list.component';
import { TodoFormComponent } from './todo-app/todo-form.component';
import { UnlessDirective } from './unless.directive';
import { HighlightDirective } from './highlight.directive';
import { LifeCycleComponent } from './life-cycle.component';
import { ShortenPipe } from './shorten.pipe';
import { AppNavComponent } from './app-nav/app-nav.component';


import { Http, Headers, BaseRequestOptions } from '@angular/http';
class MyRequestOptions extends BaseRequestOptions{
    
    merge(options){
      options.headers['Authentication'] = 'Bearer lubieplacki';
      return super.merge(options);
    }
    
}


import routerModule from './app.routing';
import { TestujemyComponent } from './testujemy/testujemy.component'

@NgModule({
  declarations: [
    AppComponent,
    /* TODO_APP_COMPONENTS */[
      TodoAppComponent,
      TodoItemComponent,
      TodoListComponent,
      TodoFormComponent
    ],
    UnlessDirective,
    HighlightDirective,
    LifeCycleComponent,
    AppNavComponent,
    TestujemyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MusicAppModule,
    routerModule
  ],
  exports:[
  ],
  providers: [
    {provide: BaseRequestOptions, useClass:MyRequestOptions }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
