import { RouterModule, Routes } from '@angular/router';
import { TodoAppComponent } from './todo-app/todo-app.component';

const routes:Routes = [
  {path:'', redirectTo:'music', pathMatch:'full'},
  {path:'todos', component: TodoAppComponent},
  {path:'**', redirectTo:'todos'}
]

const routerModule = RouterModule.forRoot(routes,{
  enableTracing:true,
  //useHash: true
})

export const routerProviders = [];
export const routerDeclarations = [];

export default routerModule
