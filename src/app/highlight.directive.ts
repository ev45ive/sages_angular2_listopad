import { Directive, ElementRef, Input, OnInit, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[highlight]',
  //host:{
    //'(mouseenter)':'onMouseEnter($event)'
    //'[style.backgroundColor]':'getColor()'
  //}
})
export class HighlightDirective implements OnInit{

  @Input('highlight')
  color = ''

  @HostBinding('style.backgroundColor')
  get getColor(){
    return (this.active? this.color : '')
  }

  active = false;

  @HostListener('mouseenter',['$event'])
  onSelected(event){
    this.active = true;
    //this.el.nativeElement.style.backgroundColor = this.color    
  }
  @HostListener('mouseleave',['$event'])
  onUnselected(event){
    this.active = false;
    //this.el.nativeElement.style.backgroundColor = ''    
  }

  ngOnInit(){
    console.log(this.color)

  }

  constructor(private el:ElementRef) {

  }

}
