import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MusicAppComponent } from './music-app.component';
import { AlbumSearchComponent } from './album-search/album-search.component';
import { AlbumListComponent } from './album-search/album-list.component';
import { AlbumCardComponent } from './album-search/album-card.component';
import { AlbumFilterComponent } from './album-search/album-filter.component';
import { MusicService, URLFactoryToken } from './music.service'

import { ShortenPipe } from '../shorten.pipe';
import { AlbumDetailsComponent } from './album-search/album-details.component';
import { TracksListComponent } from './album-search/tracks-list.component';
import { AudioPlayerComponent } from './album-search/audio-player.component';

import { RouterModule, Routes, CanActivate } from '@angular/router';
import { AlbumComponent } from './album-search/album.component';
import { AlbumViewComponent } from './album-view.component';
import { PlaylistsViewComponent } from './playlists-view.component'
import { PlaylistsService } from './playlists.service'
import { PlaylistComponent } from './playlist.component';
import { PlaylistListComponent } from './playlist-list.component'



// class CanPlaylist implements CanActivate{
//   canActivate(){
    
//   }
// }

const routes:Routes = [
  {path:'music', component: AlbumSearchComponent},
  {path:'music/album/:id', component: AlbumViewComponent,
    children:[
      {path:'',component: AlbumComponent, outlet:'side'},
      {path:'',component: AlbumSearchComponent },
    ]},
  {path:'playlists', component: PlaylistsViewComponent,
    //canActivate:[CanPlaylist],
    children:[
      {path:'', component:PlaylistComponent},
      {path:'', component:AudioPlayerComponent, outlet:'side'}
  ] }
]

const routerModule = RouterModule.forChild(routes)


const URLFactory = (query) => {
  return `https://api.spotify.com/v1/search?`
  +`q=${query}&type=album&market=PL`
}

const URLServer = 'http://localhost/api/'


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    routerModule
  ],
  declarations: [
    MusicAppComponent, 
    AlbumSearchComponent, 
    AlbumListComponent, 
    AlbumCardComponent, 
    AlbumFilterComponent,
    ShortenPipe,
    AlbumDetailsComponent,
    TracksListComponent,
    AudioPlayerComponent,
    AlbumComponent,
    AlbumViewComponent,
    PlaylistsViewComponent,
    PlaylistComponent,
    PlaylistListComponent,
  ],
  exports: [
    MusicAppComponent, 
    ShortenPipe,
    AudioPlayerComponent
  ],
  providers:[
    // { provide: MusicService, 
    //   useClass: MusicService},
    MusicService,
    PlaylistsService,
    {provide: URLFactoryToken, useValue: URLFactory},
    {provide:'Costam', useFactory: (URLServer)=>{
      return {}
    }, deps: [URLServer]}
  ]
})
export class MusicAppModule { }
