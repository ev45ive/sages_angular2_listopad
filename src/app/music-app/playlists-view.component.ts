import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'playlists-view',
  template: `
    <p>
      playlists-view Works!
    </p>
    <div class="columns">
      <div class="column">
          <router-outlet></router-outlet>        
      </div>
      <div class="column">
        <router-outlet name="side"></router-outlet>
      </div>
    </div>
  `,
  styles: []
})
export class PlaylistsViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
