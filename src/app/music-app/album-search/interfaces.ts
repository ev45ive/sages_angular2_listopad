export interface Album{
    id: string,
    href: string,
    images: AlbumImage[],
    artitsts: Artist[],
}

export interface AlbumImage{
    url: string,
    width: number,
    height: number
}

export interface Artist{
    id: string,
    name: string
}
