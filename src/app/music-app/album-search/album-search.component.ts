import { Component, OnInit } from '@angular/core';
import { MusicService } from '../music.service'
import { Album } from './interfaces'
import { Observable, Subject } from 'rxjs'

@Component({
  selector: 'album-search',
  template: `
  <div class="columns">
    <div class="column">
      <album-filter [query]="queries$" (search)="doSearch($event)"></album-filter>
    </div>
  </div>
  <div class="columns">
    <div class="column is-8">
      <album-list (selected)="selected=$event" [albums]="albums$"></album-list>
    </div>
    <div class="column is-4">
      <div>Album Details</div>
      <album-details *ngIf="selected" [album]="selected"></album-details>
    </div>
  </div>  
  `,
  styles: [],
  // providers: [],
  // viewProviders: [MusicService]
})
export class AlbumSearchComponent{

  albums:Album[] = [];
  albums$:Observable<Album[]>;
  queries$:Observable<Album[]>;

  doSearch(query){
    this.musicService.doSearch(query)
  }

  constructor(private musicService: MusicService) {
    this.albums$ = musicService.getAlbumStream()
    this.queries$ = musicService.getQueryStream()
  }
}
