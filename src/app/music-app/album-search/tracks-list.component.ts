import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MusicService } from '../music.service'
import { Album } from './interfaces'

@Component({
  selector: 'tracks-list',
  template: `
   <div class="panel">
     <div *ngFor="let track of tracks"
       (click)="select(track)"
       class="panel-block">
         <a [class.is-active]="selected == track">
        {{track.name}} </a>
     </div>
   </div>
  `,
  styles: []
})
export class TracksListComponent implements OnInit {

  @Input()
  tracks = [];

  selected;

  constructor() { }

  @Output('select')
  onSelect = new EventEmitter();

  select(track){
    this.selected = track;
    this.onSelect.emit(track);
  }

  ngOnInit() {
  }

}
