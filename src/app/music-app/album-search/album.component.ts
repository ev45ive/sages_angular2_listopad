import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MusicService } from '../music.service'
import { Album } from './interfaces'

@Component({
  selector: 'album',
  template: `   
      <div *ngIf="album">
        <h3> {{album.name}}</h3>
        <album-details [album]="album"></album-details>
      </div>
  `,
  styles: []
})
export class AlbumComponent implements OnInit {
  
  album;

  constructor(route: ActivatedRoute, private musicService: MusicService) {
      //route.snapshot.queryParams['query']
      let id = route.snapshot.parent.params['id'] 

 
      route.parent.params.subscribe(params=>{
          musicService.getAlbum(params['id']).subscribe((album)=>{
            this.album =  album;
          })
      })

  }

  ngOnInit() {
  }

}
