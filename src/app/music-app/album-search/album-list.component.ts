import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Album} from './interfaces'
import { Router } from '@angular/router';

@Component({
  selector: 'album-list',
  template: `
   <input type="range" min="3" max="6" [(ngModel)]="width">{{width}}
   <ul class="columns is-multiline">
     <li *ngFor="let album of albums | async"

      [routerLink]="['/music','album',album.id]" 
     
      [class]="'column is-'+width+(selected==album?' selected':' ')">
       <album-card [album]="album"></album-card>
     </li>
   </ul>
  `,
  styles: [`li.column {
      transition: 1s width ease-out;
  }`,`.selected{ border: 2px solid blue}`]
})
export class AlbumListComponent implements OnInit {

  width=3;

  @Input()
  albums

  @Output('selected')
  onSelected = new EventEmitter<Album>();

  selected;

  selectAlbum(album){
    //this.router.navigate(['/music','album',album.id])
    this.selected = album;
    this.onSelected.emit(this.selected)
  }

  constructor(private router:Router) { }

  ngOnInit() {
  }

}
