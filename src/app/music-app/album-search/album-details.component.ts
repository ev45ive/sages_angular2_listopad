import { Component, OnInit, Input } from '@angular/core';
import { MusicService } from '../music.service'
import { Album } from './interfaces'
import { PlaylistsService } from '../playlists.service'

@Component({
  selector: 'album-details',
  template: `
    <div *ngIf="album">
      <h3 class="title is-3">{{album.name}}</h3>
      <album-card [album]="album"></album-card>
      <tracks-list (select)="addToPlaylist($event)" [tracks]="tracks"></tracks-list>
    </div>
  `,
  styles: [`
      :host{
        /*position:fixed;
        width:200px;*/
      }
  `]
})
export class AlbumDetailsComponent implements OnInit {

  @Input('album')
  set setAlbum(album){
    if(!album) return;

    this.musicService.getAlbumTracks(album)
    .subscribe(tracks => {
      this.tracks = tracks;
    })
    this.album = album;
  }

  tracks = []

  album;

  constructor(private musicService: MusicService,
              private playlistsService: PlaylistsService) {

  }

  addToPlaylist(track){
    this.playlistsService.addTrack(track)
  }

  ngOnInit() {
  }

}
