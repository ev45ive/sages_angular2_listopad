import { Component, OnInit, Input } from '@angular/core';
import {Album, Artist, AlbumImage} from './interfaces'

@Component({
  selector: 'album-card',
  template: `
  <div>
    <img [src]="image.url" [width]="image.width">
    <h3>{{album.name | shorten : width }}</h3>
  </div>
  `,
  styles: []
})
export class AlbumCardComponent implements OnInit {

  width: 25;

  @Input('album')
  set setAlbum(album){
    this.album = album
    this.image = album.images[0]
    this.artist = album.artists[0]
  }

  album:Album;

  image:AlbumImage;

  artist:Artist;
  
  constructor() { }

  ngOnInit() {
  }

}
