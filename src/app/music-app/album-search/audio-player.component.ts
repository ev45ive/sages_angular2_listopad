import { Component, OnInit } from '@angular/core';
import { MusicService } from '../music.service'
import { Album } from './interfaces'

@Component({
  selector: 'audio-player',
  template: `
    <div *ngIf="track">
      <audio controls="true" autoplay="true" 
            [src]="track.preview_url"></audio>
    </div>
  `,
  styles: []
})
export class AudioPlayerComponent implements OnInit {
  
  track;

  constructor(private musicService: MusicService) { 
    this.musicService.playTrack$.subscribe((track)=>{
      this.track = track;
    })

  }

  ngOnInit() {
  }

}
