import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators,  AbstractControl }  from '@angular/forms';
import { MusicService } from '../music.service'
import { Observable } from 'rxjs'

interface Errors {[key:string]:any};

@Component({
  selector: 'album-filter',
  template: `
  <form [formGroup]="searchForm">
     <div class="control has-addons">
        <input formControlName="query" class="input is-expanded">
        <input type="button" value="Search" class="button">
     </div>
      {{searchForm.controls.query.valid}}
     <div *ngIf="searchForm.controls.query.dirty">
      <div *ngIf="searchForm.controls.query.errors?.minlength">
       Za krótkie
      </div>
     <div *ngIf="searchForm.controls.query.errors?.required">
       Wymagane
      </div>
     <div *ngIf="searchForm.controls.query.errors?.not_contains">
       Nie może zawierać słowa 
       {{searchForm.controls.query.errors.not_contains.word}}
      </div>
     <div *ngIf="searchForm.controls.query.errors?.async_not_contains">
       Nie może zawierać słowa (async)
       {{searchForm.controls.query.errors.async_not_contains.word}}
      </div>
    </div>

  </form>
  `,
  styles: []
})
export class AlbumFilterComponent implements OnInit {
  
  searchForm:FormGroup

  @Input('query')
  set setQuery(query){
    query.subscribe( value =>{
      //this.searchForm.get('query').setValue(value); 
    })
  }

  
  ValidatorNotContains = (word) => (
    (control:AbstractControl):Errors => {
      return control.value.match(word)? {
        'not_contains': { 'word':word }
      } : {}
    }
  )
 
  AsyncValidatorNotContains = (word) => (
    (control:AbstractControl):Observable<Errors>|Promise<Errors> => {
        // return new Promise((resolve)=>{
        //   resolve( {
        //     'async_not_contains': { 'word':word }
        //   })
        // })

      return Observable.create(subscriber => {

        //setTimeout(()=>{
          console.log('asnyc',word)
          subscriber.next(//control.value.match(word)? 
          {
            'async_not_contains': { 'word':word }
          }
          // : {}
          )

        //},2000)

      })
    }
  )
  
  constructor(private musicService: MusicService) {
   
    this.searchForm = new FormGroup({
      'query': new FormControl(musicService.defaultQuery,[
        Validators.required,
        Validators.minLength(3),
        this.ValidatorNotContains('Bieber')
      ],[
        //this.AsyncValidatorNotContains('Gaga')
      ])
    })

    console.log(this.searchForm)

    // this.searchForm.get('query').statusChanges
    // .do(n => console.log('status',n))
    // .switchMap((status) => status === "VALID"?
    //   this.searchForm.get('query').valueChanges : Observable.never())
    // .debounceTime(400)
    // .distinctUntilChanged()
    // .do(n => console.log('query',n))
    // .map(query => ('string' === typeof query) && query.trim() )
    // .subscribe(value => {
    //   this.doSearch(value)
    // })

    this.searchForm.get('query').valueChanges
    .filter(()=>this.searchForm.valid)
    .debounceTime(400)
    .map(query => ('string' === typeof query) && query.trim() )
    .filter(query => query.length >= 3)
    .distinctUntilChanged()
    .subscribe(value => {
      this.doSearch(value)
    })
 
  }

  @Output('search')
  queryEmitter = new EventEmitter()

  
  doSearch(query){
    this.queryEmitter.emit(query)
  }

  ngOnInit() {
  }

}
