import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service'
import { MusicService } from './music.service'

@Component({
  selector: 'playlist',
  template: `
    <p>
      playlist Works!
    </p>
    <tracks-list (select)="play($event)" [tracks]="tracks"></tracks-list>
  `,
  styles: []
})
export class PlaylistComponent implements OnInit {

  tracks = [];

  constructor(private musicService: MusicService,
              private playlistsService: PlaylistsService) {
      this.tracks = playlistsService.playlist;
  }

  play(track){
    this.musicService.playTrack(track)
  }



  ngOnInit() {
  }

}
