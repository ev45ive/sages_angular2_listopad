import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'album-view',
  template: `
    <div class="columns">
      <div class="column">
         <router-outlet></router-outlet>
      </div>
      <div class="column">
         <router-outlet name="side"></router-outlet>
      </div>
    </div>
  `,
  styles: []
})
export class AlbumViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
