import { Injectable } from '@angular/core';
import { Http, Headers, BaseRequestOptions } from '@angular/http';

@Injectable()
export class PlaylistsService {

  constructor(private http:Http) {
   this.http.get('http://localhost:3000/tracks')
       .map(response => response.json())
       .subscribe((tracks)=>{ 
          this.playlist = tracks
       })

  }

  playlist = []

  addTrack(track){

    this.http.post('http://localhost:3000/tracks',track
  // ,{
  //   headers: new Headers({
  //     'Authentication':'Bearer lubieplacki'
  //   })
  // }
    )
      .map(response => response.json())
      .subscribe((track)=>{ 
          this.playlist.push(track)
      })

  }

}
