import { Injectable, Inject, Injector, OpaqueToken } from '@angular/core';
import { Http, Response } from '@angular/http'
import { Album } from './album-search/interfaces'
import { Observable, Subject } from 'rxjs'
import 'rxjs/Rx'

export const URLFactoryToken = new OpaqueToken('URLFactory')

@Injectable()
export class MusicService {

  defaultQuery = '';

  constructor(private http: Http, @Inject(URLFactoryToken) private UrlFactory) {

    this.queries$.subscribe((query) => {
      this.defaultQuery = query;
      localStorage.setItem('query', query);
    });

    Observable.of(localStorage.getItem('query') || this.defaultQuery)
    .filter(x=> x!==null )
    .subscribe((query)=> this.queries$.next(query))

    this.queries$
      .filter(query=>!!query)
      .map(query => this.UrlFactory(query))
      .flatMap(url => this.http.get(url))
      .map((response: Response) => response.json())
      .map(data => data.albums.items)
      .subscribe((albums) => {
        this.albums$.next(albums)
      })

    this.albums$.subscribe((albums) => {
      this.albumsCache = albums;
      localStorage.setItem('albums', JSON.stringify(this.albumsCache));
    })

    // let oneAlbum$ = this.albums$.map(albums => albums[0])
    // oneAlbum$.subscribe(album => console.log(album))

    let $albums = Observable.of(<Album[]>JSON.parse(localStorage.getItem('albums')))
    
    $albums.filter(albums=> albums && !!albums.length )
    .subscribe((albums)=>this.albums$.next(albums))

    $albums.filter(albums=> !albums )
    .subscribe(()=>this.doSearch(this.defaultQuery))

  }

  albums$ = new Subject<Album[]>()
  queries$ = new Subject<string>()

  albumsCache: Album[] = []

  getAlbumStream() {
    return Observable.from(this.albums$)
      .startWith(this.albumsCache)
  }

  getAlbumTracks(album){
    return this.http.get(album.href)
    .map(response => response.json())
    .map(album => album.tracks.items )
  }

  getQueryStream() {
    return Observable.from(this.queries$)
      .startWith(this.defaultQuery)
  }

  playTrack$ = new Subject()
  playingTrack = null;

  playTrack(track){
    this.playingTrack = track;
    this.playTrack$.next(track)
  }

  doSearch(query) {
    this.queries$.next(query)
  }

  getAlbum(id){
    return this.http.get('https://api.spotify.com/v1/albums/'+id)
    .map(response => response.json())
  }

}
