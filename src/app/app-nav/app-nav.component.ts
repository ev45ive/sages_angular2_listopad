import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  template: `
     <nav class="nav has-shadow">
       <div class="container">

         <div class="nav-left">
           <h3 class="nav-item"> Angular 2 </h3> 

           <a routerLink="/music"    routerLinkActive="is-active" class="nav-item">Music</a>
           <a routerLink="/playlists" routerLinkActive="is-active" class="nav-item">Playlist</a>
           <a routerLink="/todos"    routerLinkActive="is-active" class="nav-item">Todos</a>
         </div>

         <div class="nav-right">     
         </div>
       </div>
     </nav>
  `,
  styles: []
})
export class AppNavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
