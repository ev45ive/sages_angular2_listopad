import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten',
  pure: true
})
export class ShortenPipe implements PipeTransform {

  transform(value: any, length = 25 ): any {
    return value.length>length?
      (value.substr(0,length) + ' ...') :
       value;
  }

}
