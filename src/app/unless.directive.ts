import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef,
   ViewRef } from '@angular/core';

@Directive({
  selector: '[unless], .unless',
  // <div unless></div>
})
export class UnlessDirective {

  cache:ViewRef;

  @Input('unless')
  set hidden(unless_attr){
    console.log(unless_attr)
   // this.el.nativeElement.style.display = unless_attr? 'none':'initial';

    if(!this.cache){
      this.cache = this.vc.createEmbeddedView(this.tf);
    }else{
      if(!unless_attr){
          this.vc.insert(this.cache)
      }else{
        this.vc.detach()
      }
    }

  };

  constructor(private tf:TemplateRef<any>, private vc:ViewContainerRef) {
    //private el:ElementRef
    //console.log(el.nativeElement.style.display)
   }

}
