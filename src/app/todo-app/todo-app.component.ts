import { Component, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import { Todo } from './interfaces'
import { TodoFormComponent } from './todo-form.component'

@Component({
  selector: 'todo-app',
  template: `
    <button (click)="show='todos'">Todos</button> 
    <button (click)="show='archived'">Archived</button>

    <div [ngSwitch]="show">
     <div *ngSwitchDefault>
        <todo-list [data]="todos">  
          <header>
            <h3> Todos!</h3>
          </header>

          <footer>
            <todo-form #form (newitem)="addTodo($event)"></todo-form>
            <input type="button" value="Archive Completed" 
            (click)="archiveCompleted()">
          </footer>
        </todo-list>
      </div>

      <div *ngSwitchCase="'archived'">
        <todo-list [data]="archived">
          <header>
            <h3> Archived Todos!</h3>
          </header>
        </todo-list>
      </div>
    </div>
  `,
  styles: [`
    .todo-item{
      background: lightblue;
    }
  `],
  //encapsulation: ViewEncapsulation.None
})
export class TodoAppComponent implements OnInit {

  @ViewChild(TodoFormComponent)
  todoForm:TodoFormComponent;

  archiveCompleted() {
    var groups = {todos:[],archived:[]}
    
    this.todos.reduce((groups, todo)=>(
      groups[ todo.completed? 'archived':'todos' ].push( todo ),groups
    ),groups)
    this.todos = groups.todos; this.archived = groups.archived
  }

  archived: Todo[] = []

  addTodo(newTodo) {
    this.todos.push(newTodo);
  }

  todos: Todo[] = [{
    name: 'Zakupy!',
    completed: true,
  }, {
    name: 'Learn NG2!',
    completed: false,
  }, {
    name: 'Profit!',
    completed: false,
  }]

  constructor() {


   }

  ngOnInit() {
  }

}
