import { Component, OnInit, Input, ElementRef} from '@angular/core';
import {Todo} from './interfaces'

@Component({
  selector: 'todo-item',
  template: `
    <span [class.todo-item]="todo.completed" >
      <span>{{todo.name}}</span>
      <input [(ngModel)]="todo.completed" type="checkbox" 
             (ngModelChange)="toggleCompleted($event)">
    </span>
  `,
  styles: []
})
export class TodoItemComponent implements OnInit {

  @Input()
  todo:Todo;

  toggleCompleted(completed){
    //...
  }
  constructor(el:ElementRef) {

  }

  ngOnInit() {
  }

}
