import { Component, OnInit, Input, ViewChildren, QueryList,
   AfterViewInit, AfterContentInit, ContentChild  } from '@angular/core';
import { TodoItemComponent } from './todo-item.component'

@Component({
  selector: 'todo-list',
  template: `
    <ng-content select="header"></ng-content>

<input type="color" [(ngModel)]="color">

    <div *ngFor="let todo of todos; let i = index" [highlight]="color">
      {{i+1}}. <todo-item [todo]="todo"></todo-item>
    </div>

    <ng-content select="footer"></ng-content>
  `,
  //inputs:[],
  styles: []
})
export class TodoListComponent implements OnInit, AfterViewInit,AfterContentInit {

  @ViewChildren(TodoItemComponent)
  todoItems = new QueryList<TodoItemComponent>()

  color = '#aaaaff'

  @Input('data')
  todos = [];

  @ContentChild('form')
  ContentChild

  ngAfterContentInit(){
    console.log(this.ContentChild)
  }

  ngAfterViewInit(){
    // (this.todoForm.newItemEmitter.subscribe((newTodo) => {
    //   console.log(newTodo)
    // }))
    this.todoItems.changes.subscribe((x) => {
      console.log(x)
    })
  }

  constructor() { }

  ngOnInit() {
  }

}
