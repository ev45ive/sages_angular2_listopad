import { Component, OnInit , Output, EventEmitter, ViewChild} from '@angular/core';
import {Todo} from './interfaces'

@Component({
  selector: 'todo-form',
  template: `
    <input type="text" #todo_name [(ngModel)]="newTodo.name" 
                       (keyup.enter)="addTodo()">
    <input type="button" value="Add" (click)="addTodo()">
  `,
  styles: []
})
export class TodoFormComponent implements OnInit {

  @Output('newitem')
  newItemEmitter = new EventEmitter<Todo>();

  newTodo:Todo = {
    name:'',
    completed: false
  }

  @ViewChild('todo_name')
  todoInput

  addTodo(name){
    // console.log(this.todoInput)
    this.newItemEmitter.emit(this.newTodo)
    this.clearTodo();
  }

  clearTodo(){
    this.newTodo = {
      name: '',
      completed: false
    }
  }
  constructor() { }

  ngOnInit() {
  }

}
