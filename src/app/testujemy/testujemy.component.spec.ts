/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed, ComponentFixtureAutoDetect, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { Http, ConnectionBackend, RequestOptions, BaseRequestOptions, Response, ResponseOptions } from '@angular/http'
import { MockBackend, MockConnection } from '@angular/http/testing'

import { TestujemyComponent } from './testujemy.component';

describe('TestujemyComponent', () => {
  let component: TestujemyComponent;
  let fixture: ComponentFixture<TestujemyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestujemyComponent ],
      imports:[

      ],
      providers: [
        MockBackend,
        BaseRequestOptions,
        {provide: Http, useFactory:( backend:ConnectionBackend, options )=>{
          return new Http(backend, options )
        },deps:[MockBackend,BaseRequestOptions]},
        //{ provide: ComponentFixtureAutoDetect, useValue: true },
        {provide: 'test', useValue: 'test'}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestujemyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  var backend:MockBackend;
  beforeEach( inject([MockBackend], (b:MockBackend)=>{
    backend = b;
  }))

  it('should get Bananas from server',(done)=>{

      backend.connections.subscribe((c:MockConnection) => {
        let response = new Response(new ResponseOptions({
          body: JSON.stringify(['Banana!'])
        }))
        c.mockRespond(response);

        backend.verifyNoPendingRequests()
        console.log(component.bananas)
        done()
      })
   
      component.getBananas()
      .subscribe((bananas)=>{
        expect(bananas).toEqual(['Banana!'])
      })
  })

  it('should call getBananas on click', () => {

    component.getBananas = jasmine.createSpy('bananaSpy')
                           .and.returnValue('Banana!')
    
    fixture
      .debugElement
      .nativeElement.querySelector('button').click()

    expect(component.getBananas).toHaveBeenCalled()

  });

  it('should render title', () => {

    expect(fixture
      .debugElement
      .nativeElement
      .querySelector('span').textContent)
    .toEqual(component.title);

    component.title = "Zmieniony"

    fixture.detectChanges()
 
    expect(fixture
      .debugElement
      .nativeElement
      .querySelector('span').textContent)
    .toEqual(component.title);

  });

  it('should have injected value', () => {
    expect(component.injected).toEqual('test');
  });

  it('should calculate values', () => {
    expect(component.calculate(1,2)).toEqual(3);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
