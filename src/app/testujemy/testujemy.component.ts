import { Component, OnInit, Inject } from '@angular/core';
import { Http } from '@angular/http'
import 'rxjs/Rx';

@Component({
  selector: 'testujemy',
  template: `
    <p>
      testujemy Works!
    </p>
    <span>{{title}}</span>
    <button (click)="getBananas($event)">Bananas!</button>
  `,
  styles: []
})
export class TestujemyComponent implements OnInit {

  injected;

  title = 'Testowy'

  constructor(@Inject('test') test, private http:Http) {
    this.injected = test;
  }

  bananas;

  getBananas(){
    // this.http.get....
    return this.http.get('bananaServer/allOfThen!')
    .map(response => response.json())
  }

  calculate(x,y){
    return x+y;
  }

  ngOnInit() {
  }

}
