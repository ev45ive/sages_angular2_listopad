import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, 
  DoCheck,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  AfterViewChecked,
  AfterContentInit,
  AfterContentChecked  
} from '@angular/core';

@Component({
  selector: 'life-cycle',
  template: `
    <h1 (click)="dummy()">LifeCycle</h1>
    {{test.test}}
    <ng-content></ng-content>
<input [(ngModel)]="test.test">
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles:[]
})
export class LifeCycleComponent implements OnInit,
  DoCheck,
  OnChanges,
  OnDestroy,
  AfterViewInit,
  AfterViewChecked,
  AfterContentInit,
  AfterContentChecked {

  @Input('data')
  test = { test: '', something:''}

  dummy(){
    console.log('changed', this.test)
    this.test.something = 'changed' 
  }

  constructor(ref: ChangeDetectorRef) {
    ref.detach();
    setInterval(() => {
      //ref.reattach()
      //ref.markForCheck()
      ref.detectChanges()
    },2000)
    console.log('constructor',arguments)
  }

  ngOnInit() {
    console.log('ngOnInit',arguments)
  }

  ngDoCheck(){
    console.log('DoCheck',arguments)
  }

  ngOnChanges(){
    console.log('OnChanges',arguments)
  }

  ngOnDestroy(){
    console.log('OnDestroy',arguments)
  }

  ngAfterViewInit(){
    console.log('AfterViewInit',arguments)
  }

  ngAfterViewChecked(){
    console.log('AfterViewChecked',arguments)
  }

  ngAfterContentInit(){
    console.log('AfterContentInit',arguments)
  }

  ngAfterContentChecked(){
    console.log('AfterContentChecked',arguments)
  }


}
